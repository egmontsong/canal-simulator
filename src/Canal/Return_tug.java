/*
 * This class describes the return tug
 * Its task is to move a vessel from the last section to the lock
 * 
 * Author: Tianlong Song
 */
package Canal;

import Canal.Lock.waterLevel;

public class Return_tug extends Thread {
	private Section section;
	private Lock lock;

	// constructor
	public Return_tug(Section section, Lock lock) {

		this.section = section;
		this.lock = lock;
	}

	// tow the vessel from last section to lock
	public synchronized void towVessel() {
		try {
			if (section.currentVessel != null) {
				if (lock.isOccupied == false// the lock is unoccupied 
					&& section.isOccupied == true //the section is occupied
					&& lock.getWaterLevel() == waterLevel.HIGH) {
					lock.occupyLock();//occupy the lock
					section.currentVessel.goDown();
						//set the vessel to go down
					System.out.println(section.currentVessel.toString()
							+ "leaves section " + (Param.SECTIONS - 1));
					Return_tug.sleep(Param.TOWING_TIME);
					lock.vesselIn(section.currentVessel);
					//the vessel enters to lock
					System.out.println(lock.currentVessel.toString()
							+ "enters lock to go down");
					section.vesselOut(section.currentVessel);
					section.freeSection();//free the last section
					lock.notify();
					section.notify();
				}			
			}
			else{
				lock.wait();
				section.wait();
				}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	//run the thread
	public void run() {
		while (true) {
			towVessel();
		}
	}

	public void interrupt() {

	}
}