/*
 * This class describes a launch tug
 * Its task is to move a vessel a tug from the lock to the first section
 * 
 * Author: Tianlong Song
 */
package Canal;

import Canal.Vessel.vesselState;

public class Launch_tug extends Thread {
	private Lock lock;
	private Section section;

	// constructor
	public Launch_tug(Lock lock, Section section) {

		this.lock = lock;
		this.section = section;
	}

	// tow a vessel to the first section
	public synchronized void towVessel() {
		try {
			if (lock.currentVessel != null) {
				if (lock.isOccupied == true
					&& //the lock is occupied
					section.isOccupied == false
					&& //the first section is unoccupied
					lock.currentVessel.getVesselState() == vesselState.goUP) {

					section.occupySection();//occupy the section
					System.out.println(lock.currentVessel.toString()
							+ "leaves lock");
					Launch_tug.sleep(Param.TOWING_TIME);//two the vessel
					section.vesselIn(lock.currentVessel);
					//vessel enters to section from lock

					System.out.println(section.currentVessel.toString()
							+ "enters section 0");
					section.currentVessel.inSECTION();
					lock.vesselOut(lock.currentVessel);
					lock.freeLock();//free the lock
					lock.notify();
					section.notify();
				}				
			}
			else{
				lock.wait();
				section.wait();
				}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	//run the thread
	public void run() {
		while (true) {
			towVessel();
		}
	}

	public void interrupt() {

	}

}