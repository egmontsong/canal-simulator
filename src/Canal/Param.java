/*
 * This class describes the public parameters 
 * 
 * Source: SWEN 40004 Assignment 2a
 * Modified by Tianlong Song
 */
package Canal;
import java.util.Random;

/**
 * Parameters that influence the behaviour of the system.
 */

public class Param {

  //the number of canal sections
  public final static int SECTIONS = 6;
  
  //the max number of vessel that the canal can process
  //it's equal to the number of sections
  public static int CANAL_CAPABILITY = SECTIONS;
  
  //the time interval at which Main checks threads are alive
  public final static int MAIN_INTERVAL = 50;

  //the time it takes to operate the lock
  public final static int OPERATE_TIME = 80;

  //the time it takes to tow
  public final static int TOWING_TIME = 120;

  //the maximum amount of time between vessel arrivals
  public final static int MAX_ARRIVE_INTERVAL = 240;

  //the maximum amount of time between vessel departures
  public final static int MAX_DEPART_INTERVAL = 80;

  //the maximum amount of time between operating the lock
  public final static int MAX_OPERATE_INTERVAL = 640;
  


/**
 * For simplicity, we assume uniformly distributed time lapses.
 * An exponential distribution might be a fairer assumption.
 */

  public static int arrivalLapse() {
    Random random = new Random();
    return random.nextInt(MAX_ARRIVE_INTERVAL);
  }

  public static int departureLapse() {
    Random random = new Random();
    return random.nextInt(MAX_DEPART_INTERVAL);
  }

  public static int operateLapse() {
    Random random = new Random();
    return random.nextInt(MAX_OPERATE_INTERVAL);
  }
}

