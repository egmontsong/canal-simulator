/*
 * This class describe the lock
 * 
 * Author: Tianlong Song
 */
package Canal;

import Canal.Vessel.vesselState;

public class Lock{
	public enum waterLevel{
		LOW,HIGH
	};//water level
	
	//water level of the lock
	public waterLevel lockWaterLevel = waterLevel.LOW;
	
	public boolean isOccupied = false;//is lock occupied
	
	public int vesselCounter = 0;//count the vesse in the canal

	Vessel currentVessel = null;//the current vessel in lock
	
	// get vessel from producer
	public synchronized void vesselIn (Vessel vessel){
		currentVessel = vessel;
	}
	
	// make the lock empty
	public synchronized void vesselOut (Vessel vessel){
		currentVessel = null;
	}
	
	//fill the chamber
	public synchronized void fillChamber(){
		while (lockWaterLevel==waterLevel.HIGH
//				||
//				(lockWaterLevel==waterLevel.LOW
//				&&
//				currentVessel.getVesselState()==vesselState.goDOWN)
				){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		lockWaterLevel=waterLevel.HIGH;
		System.out.println("Chamber fills");

	}
	
	//drain the chamber
	public synchronized void drainChamber(){
		while (lockWaterLevel==waterLevel.LOW
//				||
//				(lockWaterLevel==waterLevel.HIGH
//				&&
//				currentVessel.getVesselState()==vesselState.goUP)
				){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		lockWaterLevel=waterLevel.LOW;
		System.out.println("Chamber drains");
		notify();
	}
	
	//occupy the lock
	public synchronized void occupyLock(){
		isOccupied = true;
	}
	
	//free the lock
	public synchronized void freeLock(){
		isOccupied = false;
	}

	//get the water level of the lock
	public waterLevel getWaterLevel(){
		return lockWaterLevel;
	}
}