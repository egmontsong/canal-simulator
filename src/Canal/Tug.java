/*
 * This class describes tugs
 * Its task is to move a vessel from previous section to the next section
 * 
 * Author: Tianlong Song
 */
package Canal;

import Canal.Vessel.vesselState;

public class Tug extends Thread {
	private Section section;
	private Section section2;
	private int i;

	// constructor
	public Tug(int i, Section section, Section section2) {

		this.section = section;
		this.section2 = section2;
		this.i = i;
	}

	// tow the vessel from previous section to next section
	public synchronized void towVessel() {
		try {
			if (section.currentVessel != null) {
				if (section.isOccupied == true 
						//the section is occupied
					&& section2.isOccupied == false 
						//the next section is unoccupied
					) {
						//the vessel is going up
					section2.occupySection(); // occupy the next section
					System.out.println(section.currentVessel.toString()
							+ "leaves section " + i);
					section2.vesselIn(section.currentVessel);
					Tug.sleep(Param.TOWING_TIME);
					System.out.println(section.currentVessel.toString()
							+ "enters section " + (i + 1));
					section.vesselOut(section.currentVessel);
						// free the previous section
					section.freeSection();
					section.notify();
					section2.notify();
			}			
		}
		else{
			section.wait();
			section2.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	//run the thread
	public void run() {
		while (true) {
			towVessel();
		}

	}

	public void interrupt() {

	}
}