/*
 * This class describes the producer
 * Its task is to produce a vessel and hand it to the lock
 * 
 * Author: Tianlong Song
 */
package Canal;
 
import Canal.Lock.waterLevel;

public class Producer extends Thread {
	private Lock lock;
	private Vessel vessel;
	
	//constructor
	public Producer(Lock lock) {
		
		this.lock = lock;

	}
	
	//hands a vessel to the lock
	public synchronized void handToLock(){
		try {
			Producer.sleep(Param.arrivalLapse());
			if(lock.isOccupied == false 
					&& //the lock is empty
					lock.getWaterLevel() == waterLevel.LOW 
					&& //water level is low
					lock.vesselCounter < Param.CANAL_CAPABILITY){
					   /*
					    * the total number of vessel in the canal 
					    * must be less than the capability of the canal
					   */
				vessel = Vessel.getNewVessel();// produce a vessel
				System.out.println(vessel.toString()+ "Arrives");
				lock.occupyLock();//occupy the lock
				lock.vesselIn(vessel);//vessel enters to lock
				lock.vesselCounter++;
				System.out.println(vessel.toString()+ "enters lock to go up");
				lock.notify();
			}
			else{
				lock.wait();
				}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	//run the thread
	public void run() {
		while(true){
			handToLock();
		}			
	}

	public void interrupt() {
		
	}
}
