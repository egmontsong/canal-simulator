/*
 * This class describes the consumer
 * Its task is to move a vessel out of the lock,
 * and hand it into down stream
 * 
 * Author: Tianlong Song
 */
package Canal;

import Canal.Lock.waterLevel;
import Canal.Vessel.vesselState;

public class Consumer extends Thread {
	private Lock lock;

	//constructor
	public Consumer(Lock lock) {

		this.lock = lock;
	}

	// departs vessel to up stream
	public synchronized void departure() {
		try {
			if (lock.currentVessel != null) {// the lock is not empty
				if (lock.getWaterLevel() == waterLevel.LOW
						&& //when water level is low
						lock.isOccupied == true
						&& //lock is occupied
						lock.currentVessel.getVesselState() 
											== vesselState.goDOWN) {
						   //the vessel is going up
					Thread.sleep(Param.departureLapse());// departure lapse
					System.out.println(lock.currentVessel.toString()
							+ "departs");
					lock.freeLock();//free the lock
					lock.vesselOut(lock.currentVessel);//vessel going out
					lock.vesselCounter--;//change total vessel number in canal
					//System.out.println(lock.vesselCounter);
					lock.notify();
				}
			}
			else{
				lock.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	//run the thread
	public void run() {
		while (true) {
			departure();
		}
	}

	public void interrupt() {

	}
}