/*
 * This class describes the operator
 * Its task is inspecting  vessels which want to enter the lock,
 * and operating water level
 * 
 * Author: Tianlong Song
 */
package Canal;

import Canal.Vessel.vesselState;

public class Operator extends Thread {
	private Lock lock;

	//constructor
	public Operator(Lock lock) {
		this.lock = lock;
	}


	
	//run the thread
	public void run() {
		while (true) {
			try {
				Operator.sleep(Param.operateLapse());

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lock.drainChamber();
			try {
				Operator.sleep(Param.operateLapse());

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lock.fillChamber();
		}
	}

	public void interrupt() {

	}
}