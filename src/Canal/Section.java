/*
 * This class describes the section
 * Its task is to provide a place to berth a vessel,
 * 
 * Author: Tianlong Song
 */
package Canal;

public class Section {

	public boolean isOccupied = false;

	Vessel currentVessel = null;//the current vessel in this section
	

	public Section(int i) {
	}
	
	//get vessel from lock or previous section
	public synchronized void vesselIn (Vessel vessel){
		currentVessel = vessel;
	}
	
	//make the section empty
	public synchronized void vesselOut (Vessel vessel){
		currentVessel = null;
	}
	
	//occupy the section
	public synchronized void occupySection(){
		isOccupied = true;
	}
	
	//free the section
	public synchronized void freeSection(){
		isOccupied = false;
	}
}