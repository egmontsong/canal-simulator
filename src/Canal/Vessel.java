/*
 * This class describes vessel
 *
 * Source: SWEN 40004 Assignment 2a
 * Modified by Tianlong Song
 */
package Canal;
/**
 * A class whose instances are vessels, each with its unique Id.
 */

public class Vessel{

  //the Id of this vessel
  protected int id;

  //the next ID to be allocated
  protected static int nextId = 1;
  
  //the status of the vessel
  public enum vesselState {
	  goUP,goDOWN,inSECTIONS
  }
  
  private vesselState vesselstate = vesselState.goUP; 
  
  //create a new vessel with a given Id
  protected Vessel(int id) {
    this.id = id;
  }

  //get a new Vessel instance with a unique Id
  public static Vessel getNewVessel() {
    return new Vessel(nextId++);
  }

  //produce the Id of this vessel
  public int getId() {
    return id;
  }

  //produce an identifying string for the vessel
  public String toString() {
    return "[" + id + "] ";
  }
  
  //set the state of vessel to GoDown, when it finish the travel 
  public synchronized void goDown(){
	  vesselstate = vesselState.goDOWN;
  }
  
  //set the state of vessel to GoDown, when it finish the travel 
  public synchronized void inSECTION(){
	  vesselstate = vesselState.inSECTIONS;
  }
  
  //get the state of a vessel
  public vesselState getVesselState(){
	  return vesselstate;
  }
}

